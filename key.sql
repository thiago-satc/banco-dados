-- Desliga sistema de tabelas duplicadas
ALTER DATABASE dados SET READ_COMMITTED_SNAPSHOT OFF
ALTER DATABASE dados SET ALLOW_SNAPSHOT_ISOLATION OFF
DBCC USEROPTIONS; 

-- Mostra spid da seção atual
SELECT @@spid
-- Mosta usuarios/secoes
sp_who
-- mata secao por spid
kill 77

-- mostra looks atuais
SELECT 
	resource_type, DB_NAME(resource_database_id), 
    OBJECT_NAME(resource_associated_entity_id),
	request_mode, request_type, request_status, request_session_id 
FROM sys.dm_tran_locks WHERE resource_type = 'OBJECT'

SELECT * FROM sys.dm_tran_locks 

-- Inicia transação explicitamente
BEGIN TRAN
--Lista transações aberta
@@TRANCOUNT

-- Termina transação atual
COMMIT

-- Cancela todas as trasações abertas na seção
ROLLBACK



------------

------------






1. Escreva uma função que aceite uma data como entrada e retorne o dia da semana correspondente em formato de texto.
Por exemplo, se a entrada for "2023-05-09", a função deve retornar "terça-feira". Pesquise sobre funções de data e hora
T-SQL para responder essa questão.

CREATE OR ALTER FUNCTION get_week_day(@date date) RETURNS varchar(50) AS 
BEGIN
    return
    CASE DATEPART(weekday, @date)
    WHEN 1 THEN 'domingo'
    WHEN 2 THEN 'segunda-feira'
    WHEN 3 THEN 'terça-feira'
    WHEN 4 THEN 'quarta-feira'
    WHEN 5 THEN 'quinta-feira'
    WHEN 6 THEN 'sexta-feira'
    WHEN 7 THEN 'sabado'
    END
END;

SELECT dbo.get_week_day('2024-05-09');

2. Crie uma função que aceite um número inteiro como entrada e retorne PAR se o número for par ou ÍMPAR se o número
for ímpar.

CREATE OR ALTER FUNCTION get_even_odd(@value int) RETURNS varchar(5) AS 
BEGIN
    return CASE @value%2 WHEN 0 THEN 'PAR' ELSE 'ÍMPAR' END
END;

SELECT dbo.get_even_odd(2);

3. Escreva uma função que calcule a idade de uma pessoa com base em sua data de nascimento. A função deve aceitar a
data de nascimento como entrada e retornar um valor inteiro com a idade em anos.

CREATE OR ALTER FUNCTION get_age(@date date) RETURNS int AS 
BEGIN
    return DATEDIFF(year, @date,  GETDATE())  
END;

SELECT dbo.get_age('2000-05-10')

4. Crie uma função que aceite uma STRING como entrada e retorne a quantidade de caracteres dessa STRING. Por exemplo,
se a entrada for "Centro Universitário UNISATC", a função deve retornar 28. Pesquise sobre funções Strings T-SQL para
responder essa questão.

CREATE OR ALTER FUNCTION get_sizeof(@string text) RETURNS int AS 
BEGIN
    return DATALENGTH(@string) 
END;

SELECT dbo.get_sizeof('Centro Universitário UNISATC')

5. Crie uma função que aceite duas datas como entrada e retorne o número de dias entre essas datas. Por exemplo, se as
datas de entrada forem "2023-05-09" e "2023-05-15", a função deve retornar 6. Pesquise sobre funções de data e hora
T-SQL para responder essa questão.

CREATE OR ALTER FUNCTION get_diff_days(@start_date date, @end_date date) RETURNS int AS 
BEGIN
    return DATEDIFF(day, @start_date, @end_date) 
END;

SELECT dbo.get_diff_days('2023-05-09', '2023-05-15')

6. Crie uma função que, passando uma placa de carro como parâmetro de entrada, a função retorne a quantidade de
sinistros dessa placa. Tratar possíveis resultados NULL.
Por fim, execute um SELECT na tabela carro (mostrando todos os campos), colocando uma chamada da função no SELECT
LIST de sua consulta para que ela mostre todos os dados da tabela carro e ao final de todas as colunas, informe a
quantidade de sinistros para cada placa.

CREATE OR ALTER FUNCTION get_carro_sinistro(@placa varchar(10)) RETURNS int AS 
BEGIN
    return COALESCE((SELECT COUNT(*) FROM sinistro WHERE placa = @placa), 0)
END;

SELECT *, dbo.get_carro_sinistro(placa) AS qt_sinistro
FROM carro

7. Crie uma função que, passando um código de cliente como parâmetro de entrada, a função retorne a quantidade de
apólices vencidas desse cliente. Tratar possíveis resultados NULL.
Por fim, execute um SELECT na tabela cliente (mostrando todos os campos), colocando uma chamada da função no
SELECT LIST de sua consulta para que ela mostre todos os dados da tabela cliente e ao final de todas as colunas, informe
a quantidade de apólices vencidas para cada cliente.

CREATE OR ALTER FUNCTION get_cliente_apolice_vencidas(@cod_cliente int) RETURNS int AS 
BEGIN
    return COALESCE((SELECT COUNT(*) FROM apolice WHERE cod_cliente = @cod_cliente AND data_fim_vigencia < GETDATE()), 0)
END;

SELECT *, dbo.get_cliente_apolice_vencidas(cod_cliente) AS qt_apolices_vencidas
FROM cliente

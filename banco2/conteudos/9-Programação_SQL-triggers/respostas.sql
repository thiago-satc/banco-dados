--DROP TABLE IF EXISTS conta;
CREATE TABLE conta(
    id_conta int NOT NULL,
    nome varchar(100) NOT NULL,
    status bit NOT NULL,
);

-- true ativo false inativo
--DROP TABLE IF EXISTS conta;
CREATE TABLE lancamento(
    id_conta int NOT NULL,
    data_operacao date NOT NULL,
    descricao varchar(100) NOT NULL,
    tipo_operacao char(1) NOT NULL CHECK(tipo_operacao in ('D', 'C')),
    valor_operacao numeric(12,2) NOT NULL
);

-- credito C; debito D
--DROP TABLE IF EXISTS conta;
CREATE TABLE saldo(
    id_conta int NOT NULL,
    data_saldo date NOT NULL,
    valor numeric(12,2) NOT NULL
);

ALTER TABLE conta ADD CONSTRAINT pk_conta PRIMARY KEY (id_conta);
ALTER TABLE lancamento ADD CONSTRAINT fk_lancamento_conta FOREIGN KEY (id_conta) REFERENCES conta (id_conta);  
ALTER TABLE saldo ADD CONSTRAINT fk_saldo_conta FOREIGN KEY (id_conta) REFERENCES conta (id_conta);  



CREATE OR ALTER TRIGGER trig_lancamento ON lancamento AFTER INSERT AS
BEGIN

    IF (ROWCOUNT_BIG() = 0) RETURN;
    DECLARE @idConta int, @valor numeric(12,2), @tipo char(1), @data date = getdate();

    SELECT @idConta = id_conta, @valor = valor_operacao, @tipo = tipo_operacao FROM inserted;

    IF (@tipo = 'D')
        SET @valor = (-@valor);

    IF ((SELECT 1 FROM saldo WHERE id_conta = @idConta AND data_saldo = @data) = 1)
    BEGIN
        UPDATE saldo SET valor = valor + @valor WHERE id_conta = @idConta AND data_saldo = @data;
        
        return;
    END

    INSERT INTO saldo VALUES (@idConta, @data, @valor); 
END;

-- procedures
CREATE OR ALTER PROCEDURE add_lancamento(
    @id_conta int, 
    @descricao varchar(100), 
    @tipo_operacao char(1), 
    @valor_operecao numeric(12,2)
) AS 
BEGIN
    DECLARE @erro int = 0;
    SELECT @erro = 1 FROM conta WHERE status = 1 AND id_conta = @id_conta;
    IF (@erro != 1) 
    BEGIN
        THROW 50000,'Conta inexistente ou inativa',1;
        --RAISERROR('Conta inexistente ou inativa', 16, 1);
        --RETURN;
    END 
    INSERT INTO lancamento VALUES (
        @id_conta,
        getdate(),
        @descricao,
        @tipo_operacao,
        @valor_operecao
    );
END;

exec add_lancamento 1, 'awd', 'C', 10.0


CREATE OR ALTER PROCEDURE change_lancamento(
    @id_conta_saida int, 
    @id_conta_entrada int, 
    @valor_operacao numeric(12,2)
) AS 
BEGIN
    DECLARE @erro int = 0, @erro2 int = 0;
    SELECT @erro = 1 FROM conta WHERE status = 1 AND id_conta = @id_conta_saida;
    SELECT @erro2 = 1 FROM conta WHERE status = 1 AND id_conta = @id_conta_entrada;
    IF (@erro = 0 or @erro2 = 0) 
    BEGIN
        THROW 50000,'Conta inexistente/inativa ou saldo indisponivel',1;
        --RAISERROR('Conta inexistente ou inativa', 16, 1);
        --RETURN;
    END 
    INSERT INTO lancamento VALUES 
        (@id_conta_saida, getdate(), CONCAT('transferencia para ', @id_conta_entrada), 'D', @valor_operacao);
    INSERT INTO lancamento VALUES 
        (@id_conta_entrada, getdate(), CONCAT('transferencia de ', @id_conta_saida), 'C', @valor_operacao);
END;


exec change_lancamento 1,2, 20.0
select * from lancamento
select * from saldo




3. Criar uma tabela de log com data/hora da operação, tipo da operação (D = delete, I = insert, U = update),
nome do usuário e um campo para descrição.
Esta tabela deverá guardar todos os dados/eventos/informações para uma eventual auditoria de modificação
dos dados das tabelas conta, lancamento e saldo (comandos de INSERT, UPDATE e DELETE)
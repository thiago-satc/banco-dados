az group list

az sql server create --name batata1 --resource-group learn-0a65b92d-2fb3-42a9-97bf-7c68d28a0ff7 --location "Brazil South" --admin-user admin_user --admin-password satc@2023

az sql server firewall-rule create --resource-group learn-0a65b92d-2fb3-42a9-97bf-7c68d28a0ff7 --server batata1 -n AllowYourIp --start-ip-address 177.54.50.222 --end-ip-address 177.54.50.222

az sql db create --resource-group learn-0a65b92d-2fb3-42a9-97bf-7c68d28a0ff7 --server batata1 --name dados --edition GeneralPurpose --compute-model Serverless --family Gen5 --capacity 2 --zone-redundant false --backup-storage-redundancy "Local"
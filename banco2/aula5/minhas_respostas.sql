-- 1
BEGIN TRAN;
SELECT @@trancount;
INSERT INTO carro VALUES (
    'RLC3834',
    'TRACKER',
    '269874161875',
    'GM',
    '2021',
    'BRANCO' 
);

-- 2
-- Não recebeu um block e ficou aguardando
SELECT * FROM carro

- 3
-- NOTA: Para saber o ID da sessão atual, execute SELECT @@spid. 
-- NOTA 2: Para ver os bloqueios do SQL Server, execute sp_who. Vejas as colunas spid e blk

--Seção 85 questão 1 tem um eXclusive lock na tabela carro, imepdindo o select da seção 79 questão 2

SELECT resource_type,
       DB_NAME(resource_database_id),
       OBJECT_NAME(resource_associated_entity_id),
	request_mode,
	request_type,
	request_status,
	request_session_id
FROM sys.dm_tran_locks
WHERE resource_type = 'OBJECT';

SELECT * FROM sys.dm_tran_locks 
where resource_type = 'OBJECT';

-- 4
-- Sim foi possivel ler a tabela]
-- Porque usamos o comando NOLOCK que ignora locks
-- Sim e a tabela aprensenta o valor do insert
SELECT * FROM carro WITH (NOLOCK)

-- 5
-- Não foi adicionado porque a transação foi cancelada 
SELECT * FROM carro

-- 6
-- Sim teria sido adicionado e poderia ser consultado sem o auxilio de NOLOCK e pararia o lock da tabela, permitindo outras consultas e inserts

-- 7

-- 8
BEGIN TRAN;
SELECT @@TRANCOUNT;
SELECT * FROM MICRODADOS_ENEM_2022_SC WITH(HOLDLOCK);

-- 9
-- Não porque o select foi feito em ENEM com HOLDLOCK, bloqueia updates, se esse valor não for fornecido ele não bloqueia update
BEGIN TRAN;
SELECT @@TRANCOUNT;

UPDATE MICRODADOS_ENEM_2022_SC
SET NU_ANO = 2001
WHERE NU_INSCRICAO = 210055535854

-- 10
-- Sim rodou pq essa é a transação que está travando a tabela primeiro

-- 11
-- Ele tomou block e está aguardando a seção 4, assim ele não vai impactar em nada na tabela até a 4 terminar
SELECT resource_type,
       DB_NAME(resource_database_id),
       OBJECT_NAME(resource_associated_entity_id),
	request_mode,
	request_type,
	request_status,
	request_session_id
FROM sys.dm_tran_locks
WHERE resource_type = 'OBJECT';
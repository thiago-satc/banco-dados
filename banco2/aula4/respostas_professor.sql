
--1.
select * from MICRODADOS_ENEM_2021_SC;
go

--2. Table Scan. Este operador indica que a tabela nao
--tem um indice clustered. Indica-se a criacao de um indice
--clustered para evitar o operador table scan.


--3.
sp_helpindex MICRODADOS_ENEM_2021_SC;
go


--4.
alter table MICRODADOS_ENEM_2021_SC alter column NU_INSCRICAO bigint NOT NULL;
go

alter table MICRODADOS_ENEM_2021_SC add constraint pk_MICRODADOS_ENEM_2021_SC primary key (NU_INSCRICAO);
go

sp_helpindex MICRODADOS_ENEM_2021_SC;
go

--5.
select * from MICRODADOS_ENEM_2021_SC;
go
--Clustered Index Scan

--6.
SELECT * FROM MICRODADOS_ENEM_2021_SC WHERE NO_MUNICIPIO_ESC = 'Treviso';
go
--Nenhuma diferenÃ§a


--7.
SET STATISTICS IO ON;
SET STATISTICS TIME ON;
go

SELECT * FROM MICRODADOS_ENEM_2021_SC WHERE NO_MUNICIPIO_ESC = 'Treviso';
go

--Table 'MICRODADOS_ENEM_2021_SC'. Scan count 1, logical reads 2981, physical reads 0, page server reads 0, read-ahead reads 42, page server read-ahead reads 0, lob logical reads 0, lob physical reads 0, lob page server reads 0, lob read-ahead reads 0, lob page server read-ahead reads 0.

-- SQL Server Execution Times:
--   CPU time = 0 ms,  elapsed time = 38 ms.

--8.
SELECT COUNT(1)
FROM MICRODADOS_ENEM_2021_SC WHERE NO_MUNICIPIO_ESC = 'Treviso';
go

SELECT COUNT(1)
FROM MICRODADOS_ENEM_2021_SC;
go

--A consulta que retorna 10 linhas e a consulta que retorna 26394 linhas
--precisam ler a mesma quantidade de pÃ¡gina de dados.
--ou seja, com WHERE ou SEM WHERE o "peso" de ambas as consultas
--Ã© o mesmo.

--Table 'MICRODADOS_ENEM_2021_SC'. Scan count 1, logical reads 2981, physical reads 0, page server reads 0, read-ahead reads 0, page server read-ahead reads 0, lob logical reads 0, lob physical reads 0, lob page server reads 0, lob read-ahead reads 0, lob page server read-ahead reads 0.
--Table 'MICRODADOS_ENEM_2021_SC'. Scan count 1, logical reads 2981, physical reads 0, page server reads 0, read-ahead reads 0, page server read-ahead reads 0, lob logical reads 0, lob physical reads 0, lob page server reads 0, lob read-ahead reads 0, lob page server read-ahead reads 0.

--Ambos fazem Clustered Index Scan

--9.
create index idx_NO_MUNICIPIO_ESC on MICRODADOS_ENEM_2021_SC (NO_MUNICIPIO_ESC);
go

--verifica os indices existentes da tabela, caso exista
sp_helpindex MICRODADOS_ENEM_2021_SC


--10.
--Gerar os planos de acesso e os dados de set statistics para ambas as consultas.
SELECT NO_MUNICIPIO_ESC, TP_ESCOLA
FROM MICRODADOS_ENEM_2021_SC WHERE NO_MUNICIPIO_ESC = 'Treviso';
go

SELECT NO_MUNICIPIO_ESC, TP_ESCOLA
FROM MICRODADOS_ENEM_2021_SC;
go

--11.
drop index MICRODADOS_ENEM_2021_SC.idx_NO_MUNICIPIO_ESC;
go
create index idx_NO_MUNICIPIO_ESC on MICRODADOS_ENEM_2021_SC (NO_MUNICIPIO_ESC) INCLUDE (TP_ESCOLA);
go

--Gerar os planos de acesso e os dados de set statistics para ambas as consultas.
SELECT NO_MUNICIPIO_ESC, TP_ESCOLA
FROM MICRODADOS_ENEM_2021_SC WHERE NO_MUNICIPIO_ESC = 'Treviso';
go

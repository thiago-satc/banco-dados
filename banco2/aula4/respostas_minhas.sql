set statistics io on; --on liga e off desliga
set statistics time on; --on liga e off desliga

set statistics io off; --on liga e off desliga
set statistics time off; --on liga e off desliga

-- 1
SELECT * FROM MICRODADOS_ENEM_2021_SC;

-- 2
-- 100% no table Scan   
-- Adicionar um indice cluster
-- Adicionar indices não cluster

-- 3
-- The object 'MICRODADOS_ENEM_2021_SC' does not have any indexes, or you do not have permissions.
-- Não tem indices
sp_helpindex MICRODADOS_ENEM_2021_SC

-- 4
ALTER TABLE MICRODADOS_ENEM_2021_SC ADD CONSTRAINT pk_MICRODADOS_ENEM_2021_SC PRIMARY KEY (NU_INSCRICAO);
sp_helpindex MICRODADOS_ENEM_2021_SC
-- pk_MICRODADOS_ENEM_2021_SC; clustered, unique, primary key; NU_INSCRICAO

-- 5
SELECT * FROM MICRODADOS_ENEM_2021_SC;
-- logical reads 3022 (sem pk)
-- logical reads 2977 (com pk)
-- Clustered index scan

-- 6
SELECT * FROM MICRODADOS_ENEM_2021_SC WHERE NO_MUNICIPIO_ESC = 'Treviso'
-- Não

-- 7 
set statistics io on;
set statistics time on;
SELECT * FROM MICRODADOS_ENEM_2021_SC WHERE NO_MUNICIPIO_ESC = 'Treviso'
-- SQL Server parse and compile time: 
--    CPU time = 0 ms, elapsed time = 0 ms.
-- SQL Server parse and compile time: 
--    CPU time = 0 ms, elapsed time = 0 ms.

-- (10 rows affected)
-- Table 'MICRODADOS_ENEM_2021_SC'. Scan count 1, 
-- logical reads 2977, physical reads 0, page server reads 0, read-ahead reads 0, 
-- page server read-ahead reads 0, lob logical reads 0, lob physical reads 0, lob page server reads 0, 
-- lob read-ahead reads 0, lob page server read-ahead reads 0.

--  SQL Server Execution Times:
--    CPU time = 0 ms,  elapsed time = 6 ms.

-- Completion time: 2024-03-19T21:46:51.4135835-03:00

-- 8
-- 26394 Rows
-- logical reads 2977
--  SQL Server Execution Times: CPU time = 79 ms,  elapsed time = 1728 ms.
-- Clustered Index Scan
SELECT * FROM MICRODADOS_ENEM_2021_SC 

-- 10 rows
-- logical reads 2977
--  SQL Server Execution Times: CPU time = 0 ms,  elapsed time = 5 ms.
-- Clustered Index Scan
SELECT * FROM MICRODADOS_ENEM_2021_SC WHERE NO_MUNICIPIO_ESC = 'Treviso'

-- 9
-- logical reads 2976
CREATE INDEX idx_NO_MUNICIPIO_ESC ON MICRODADOS_ENEM_2021_SC (NO_MUNICIPIO_ESC);

-- idx_NO_MUNICIPIO_ESC; nonclustered, NO_MUNICIPIO_ESC
sp_helpindex MICRODADOS_ENEM_2021_SC

-- 10
-- 10 Rows
-- logical reads 32
-- SQL Server Execution Times: CPU time = 0 ms,  elapsed time = 0 ms.
-- Index Seek (NonClustered)
-- Key Lookup (Clustered)
SELECT NO_MUNICIPIO_ESC, TP_ESCOLA 
FROM MICRODADOS_ENEM_2021_SC WHERE NO_MUNICIPIO_ESC = 'Treviso'

-- 26394 rows
-- ogical reads 2977
-- SQL Server Execution Times: CPU time = 0 ms,  elapsed time = 8 ms.
-- Clustered Index Scan
SELECT NO_MUNICIPIO_ESC, TP_ESCOLA
FROM MICRODADOS_ENEM_2021_SC;

-- 11
SELECT NO_MUNICIPIO_ESC, TP_ESCOLA
FROM MICRODADOS_ENEM_2021_SC WHERE NO_MUNICIPIO_ESC = 'Treviso';
--
SELECT NO_MUNICIPIO_ESC, TP_ESCOLA
FROM MICRODADOS_ENEM_2021_SC;
--
SELECT NO_MUNICIPIO_ESC
FROM MICRODADOS_ENEM_2021_SC WHERE NO_MUNICIPIO_ESC = 'Treviso';
--
DROP INDEX idx_NO_MUNICIPIO_ESC ON MICRODADOS_ENEM_2021_SC
CREATE INDEX idx_NO_MUNICIPIO_ESC ON MICRODADOS_ENEM_2021_SC (NO_MUNICIPIO_ESC) INCLUDE (TP_ESCOLA)

SELECT NO_MUNICIPIO_ESC, TP_ESCOLA
FROM MICRODADOS_ENEM_2021_SC WHERE NO_MUNICIPIO_ESC = 'Treviso';


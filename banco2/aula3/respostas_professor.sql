--1.	Data a com consulta abaixo, altere o cod_cliente para informar o nome do cliente. Para isso, utilize subconsulta na lista de select.

SELECT cod_apolice
      ,(SELECT nome FROM cliente WHERE cliente.cod_cliente = apolice.cod_cliente) as nome
      ,data_inicio_vigencia
      ,data_fim_vigencia
      ,valor_cobertura
      ,valor_franquia
      ,placa
  FROM apolice
ORDER BY data_fim_vigencia ASC
GO


--2.	Data a com consulta abaixo, altere o cod_cliente para informar o nome do cliente. Para isso, utilize Join (escolha o tipo de JOIN mais edequado).

SELECT cod_apolice
      ,nome
      ,data_inicio_vigencia
      ,data_fim_vigencia
      ,valor_cobertura
      ,valor_franquia
      ,placa
  FROM apolice INNER JOIN cliente ON apolice.cod_cliente = cliente.cod_cliente
ORDER BY data_fim_vigencia ASC
GO

--3.	Faça uma consulta na tabela sinistro (com todas as colunas) adicionando uma nova coluna ordem ao final do select list, (não é para criar uma coluna com ALTER TABLE, usar somente SELECT), com um contador do número da linha, ORDENADO pelo campo local_sinistro. Nota: Utilize uma função de janela ROW_NUMBER.
select *
      ,ROW_NUMBER() OVER(ORDER BY local_sinistro) as ordem
from sinistro;
go

--4.	Faça uma consulta na tabela sinistro (com todas as colunas) adicionando uma nova chamada ordem ao final do select list, (não é para criar uma coluna com ALTER TABLE, usar somente SELECT), com um contador do número da linha, ORDENADO pelo campo local_sinistro. Nota: Utilize uma função de janela RANK.
select *
      ,RANK() OVER(ORDER BY local_sinistro) as ordem
from sinistro;
go

--5.	Faça uma consulta na tabela sinistro (com todas as colunas) adicionando uma nova chamada ordem ao final do select list, (não é para criar uma coluna com ALTER TABLE, usar somente SELECT), com um contador do número da linha, ORDENADO pelo campo local_sinistro. Nota: Utilize uma função de janela DENSE_RANK.
select *
      ,DENSE_RANK() OVER(ORDER BY local_sinistro) as ordem
from sinistro;
go

--6.	Faça uma consulta que mostre quais carros (listar todos os dados dos carros – todas as colunas da tabela carro) possuem mais de (1) um sinistro. Nota: O resultado da consulta deve mostrar todos os dados dos carros e mais uma coluna ao final com a quantidade de sinistros por cada carro.
--1a opcao
with cte as (
  select DISTINCT c.[placa]
      ,c.[modelo]
      ,c.[chassi]
      ,c.[marca]
      ,c.[ano]
      ,c.[cor]
       ,COUNT(s.placa) over (partition by c.placa order by c.placa)  as qtde_sinistro
  from carro c inner join sinistro s ON c.placa = s.placa
)
select * from cte where qtde_sinistro > 1

--2a opcao
select c.placa, modelo, chassi, marca, ano, cor, count(1) as qtde
from carro c inner join sinistro s ON c.placa = s.placa
group by c.placa, modelo, chassi, marca, ano, cor
having count(1) > 1
go

--7.	Com base na consulta anterior, adicione uma coluna ao final do select list informando quando ocorreu o primeiro sinistro (menor data).
--1a opcao
with cte as (
  select DISTINCT c.placa
      ,c.modelo
      ,c.chassi
      ,c.marca
      ,c.ano
      ,c.cor
      ,COUNT(s.placa) over (partition by c.placa order by c.placa)  as qtde_sinistro
      ,FIRST_VALUE(data_sinistro) OVER(PARTITION BY c.placa ORDER BY data_sinistro) as primeiro_sinistro
  from carro c inner join sinistro s ON c.placa = s.placa
)
select * from cte where qtde_sinistro > 1;
go

--2a opcao
with cte as (
  select c.placa, modelo, chassi, marca, ano, cor, count(1) as qtde
  from carro c inner join sinistro s ON c.placa = s.placa
  group by c.placa, modelo, chassi, marca, ano, cor
  having count(1) > 1
)
select distinct cte.placa, modelo, chassi, marca, ano, cor, qtde
      ,FIRST_VALUE(data_sinistro) OVER(PARTITION BY cte.placa ORDER BY data_sinistro) as primeiro_sinistro
FROM cte INNER JOIN sinistro ON cte.placa = sinistro.placa
go

--8.	Altere a consulta anterior, adicionando uma outra coluna ao final do select list (preservar a coluna criada pela questão 7) informando quando ocorreu o último sinistro (menor data).
--1a opcao
with cte as (
  select DISTINCT c.placa
      ,c.modelo
      ,c.chassi
      ,c.marca
      ,c.ano
      ,c.cor
      ,COUNT(s.placa) over (partition by c.placa order by c.placa)  as qtde_sinistro
      ,FIRST_VALUE(data_sinistro) OVER(PARTITION BY c.placa ORDER BY data_sinistro) as primeiro_sinistro
	  ,LAST_VALUE(data_sinistro) OVER(PARTITION BY c.placa ORDER BY data_sinistro ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as ultimo_sinistro
  from carro c inner join sinistro s ON c.placa = s.placa
)
select * from cte where qtde_sinistro > 1;
go

--2a opcao
with cte as (
  select c.placa, modelo, chassi, marca, ano, cor, count(1) as qtde
  from carro c inner join sinistro s ON c.placa = s.placa
  group by c.placa, modelo, chassi, marca, ano, cor
  having count(1) > 1
)
select distinct cte.placa, modelo, chassi, marca, ano, cor, qtde
      ,FIRST_VALUE(data_sinistro) OVER(PARTITION BY cte.placa ORDER BY data_sinistro) as primeiro_sinistro
	  ,LAST_VALUE(data_sinistro) OVER(PARTITION BY cte.placa ORDER BY data_sinistro ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as ultimo_sinistro
FROM cte INNER JOIN sinistro ON cte.placa = sinistro.placa;
go

--9.	Gere uma consulta listando o nome da região e o nome dos estados vinculados a cada região (resultado com 2 colunas) ordenados pelo nome da região e pelo nome do estado respectivamente. Gerar duas opções de consulta. Uma com JOIN e a outra com subconsultas no select list.
--1a opcao
SELECT nm_regiao, nm_estado
FROM regiao inner join estado on regiao.cd_regiao = estado.cd_regiao
ORDER BY nm_regiao, nm_estado
go

--2a opcao
SELECT (SELECT nm_regiao from regiao where estado.cd_regiao = regiao.cd_regiao) as nm_regiao
       ,nm_estado
FROM estado
WHERE estado.cd_regiao is not null
ORDER BY nm_regiao, nm_estado
go

--10.	Utilizando a consulta gerada na questão anterior, faça as alterações necessárias na consulta para informar o nome do estado que está na 5a posição do ranking.
--1a opcao
with cte as (
  SELECT nm_regiao, nm_estado, ROW_NUMBER() OVER (ORDER BY nm_regiao, nm_estado) as posicao
  FROM regiao inner join estado on regiao.cd_regiao = estado.cd_regiao
)
select * from cte where posicao = 5
go

--2a opcao
with cte as (
  SELECT (SELECT nm_regiao from regiao where estado.cd_regiao = regiao.cd_regiao) as nm_regiao
         ,nm_estado
  FROM estado
  WHERE estado.cd_regiao is not null
),
cte1 as (
  select * ,ROW_NUMBER() OVER (ORDER BY nm_regiao, nm_estado) as posicao
  from cte
)
select * from cte1 where posicao = 5
go

--3a opcao
with cte as (
  SELECT nm_regiao, nm_estado, RANK() OVER (ORDER BY nm_regiao, nm_estado) as posicao
  FROM regiao inner join estado on regiao.cd_regiao = estado.cd_regiao
)
select * from cte where posicao = 5
go

--4a opcao
with cte as (
  SELECT (SELECT nm_regiao from regiao where estado.cd_regiao = regiao.cd_regiao) as nm_regiao
         ,nm_estado
  FROM estado
  WHERE estado.cd_regiao is not null
),
cte1 as (
  select * ,RANK() OVER (ORDER BY nm_regiao, nm_estado) as posicao
  from cte
)
select * from cte1 where posicao = 5
go


--11.	Desafio: Gere uma consulta na tabela apolice (listas todas as colunas), alterando a coluna cod_cliente Informando o nome do cliente (usar uma subconsulta no select list para isso).
--Utilizando essa consulta como base, faça as alterações necessárias para adicionar uma nova coluna ao final do select list com o valor acumulado da coluna valor_franquia, participado por cliente, e ordenado pelo nome do cliente e pelo código da apólice respectivamente.

--1a opcao - subconsulta inteira dentro do OVER da WINDOW FUNCTION
SELECT cod_apolice
      ,(SELECT nome FROM cliente WHERE cliente.cod_cliente = apolice.cod_cliente) as nome
      ,data_inicio_vigencia
      ,data_fim_vigencia
      ,valor_cobertura
      ,valor_franquia
      ,placa
	  ,SUM(valor_franquia) OVER (
		PARTITION BY (SELECT nome FROM cliente WHERE cliente.cod_cliente = apolice.cod_cliente)
		ORDER BY (SELECT nome FROM cliente WHERE cliente.cod_cliente = apolice.cod_cliente), cod_apolice) as valor_acumulado
  FROM apolice
GO

--2a opcao
with cte as (
  SELECT cod_apolice
      ,(SELECT nome FROM cliente WHERE cliente.cod_cliente = apolice.cod_cliente) as nome
      ,data_inicio_vigencia
      ,data_fim_vigencia
      ,valor_cobertura
      ,valor_franquia
      ,placa
  FROM apolice
)
select *
       ,SUM(valor_franquia) OVER (PARTITION BY nome ORDER BY nome, cod_apolice) as valor_acumulado
from cte



Questão:1
Marque VERDADEIRO ou FALSO para as afirmações abaixo e depois escolha a alternativa que corresponde a sequência correta.


(  F  ) Um índice nonclustered é criado em uma coluna ou conjunto de colunas que são frequentemente usadas para consultas. Neste tipo de índice os valores das colunas chaves não podem se repetir, caso contrário o índice não irá funcionar.

(  V  ) No nível folha de um índice clustered encontramos todos as colunas da tabela. A chave deste índice pode ser simples ou composta.

(  V  ) Um índice nonclustered é criado em uma ou mais colunas que são chamadas de chave do índice.

(  V  ) Uma tabela que não possui índice clustered é chamada de tabela HEAP.

(  F  ) Um índice com uma chave simples pode ter mais de uma coluna como chave.

(  V  ) Um índice de cobertura (criado com INCLUDE) é um índice que cobre uma lista de select
resolvendo problemas de lookup, e normalmente é utilizado com índices clustered.

e) F, V, F, F, F, F
a) F, V, V, V, F, F


b) - V, V, V, V, F, F

c) - F, F, V, V, F, V

d) - V, F, V, V, F, V



f)-V, V, F, F, F, F

g)-  F, F, F, F, F, V

h) - V, F, F, F, F, V



Questão:2
Com base no diagrama abaixo, faça um comando SELECT que traga o nome de TODOS os cursos e a quantidade de alunos cadastrados em cada curso, ordenados de forma decrescente pela quantidade de alunos.

WITH cte AS (
SELECT 
	cu.nm_curso, 
	(
		SELECT COUNT(*) FROM aluno al WHERE al.cd_curso = cu.cd_curso
	) as contagemalunos
FROM curso cu
)

SELECT * FROM cte ORDER BY contagemalunos DESC;

-- 3
WITH cte AS (
	SELECT pr.nm_profissional,
		( SELECT COUNT(*) FROM agenda ag WHERE ag.cd_profissional = pr.cd_profissional) as qt
	FROM profissional pr
), cte2 AS (
	SELECT cte.*, DENSE_RANK() OVER(ORDER BY qt) as rank
	FROM cte

)

SELECT * FROM cte2 WHERE rank <=3

-- 4

-- 5
Questão:5
Marque VERDADEIRO ou FALSO para as afirmações abaixo e depois escreva a resposta correta. 

NOTA: Essa questão somente será considerada CORRETA (peso total) se TODAS as alternativas estivem marcadas corretas.
 
(  v   ) Deadlock é evento que ocorre quando temos duas transações bloqueadas permanentemente, incapazes de continuar porque cada uma está esperando pela outra para liberar um recurso necessário.

(  v   ) Todo comando de SELECT gera lock.

(  v   ) Todo comando de UPDATE gera lock.

(  f   ) Eventos de lock só ocorrem dentro de transações explícitas, ou seja, que foram iniciadas por BEGIN TRAN.

(   f  ) Lock exclusivo é marcado em uma tabela que sofreu uma operação de DQL.

(   f  ) Dentro da mesma sessão que gerou um lock compartilhado em uma tabela, é possível ler os dados dessa tabela, mas não é possível alterá-los.

(  v  ) Somente é possível efetuar leitura suja em uma tabela (ler dados da tabela bloqueados por uma outra transação ainda não finalizada) com um HINT chamado NOLOCK com lock exclusivo.

-- 6
Crie uma consulta que informe quantos carros não possuem sinistro.
- Utilize apenas SUBCONSULTA ANINHADA para a sua resposta.
- Subconsultas correlacionadas ou junções não serão aceitas como resposta válida.

SELECT COUNT(*) AS sem_sinistro FROM carro ca
WHERE ca.placa NOT IN (
	SELECT placa FROM sinistro GROUP BY placa
);

-- 7 
-- Desse jeito para não usar top 1, se vir a ter mais de uma placa vai aparecer
-- dava para fazer com agregada tbm
WITH cte AS (
	SELECT placa, count(*) AS qt
	FROM sinistro 
	GROUP BY placa 
), cte2 AS (
	SELECT MAX(qt) AS maior 
	FROM cte 
)

SELECT * FROM cte WHERE qt = (SELECT * FROM cte2)

-- 8
SELECT nm_paciente
FROM paciente
WHERE dt_nascimento = '2000-06-15';
set statistics io on
sp_helpindex paciente
DROP index idx_paciente1 ON paciente

CREATE index idx_paciente1 ON paciente (dt_nascimento) include (nm_paciente);

-- 9
SELECT COUNT(*) AS sem_sinistro FROM carro ca
WHERE NOT EXISTS (
	SELECT placa FROM sinistro si WHERE ca.placa = si.placa
);

-- 10 
Coluna 1

1 – Junção

2 – Subconsulta

3 – CTE

4 – Shared

5 – Bloqueio

6 – Windows Functions
 

 
Coluna 2

(  6   ) Utilizado para trabalhar com dados agregados, analíticos etc., sem agrupar as linhas.

(  1  ) Operação utilizada para combinar dados de duas ou mais tabelas com base em uma condição de associação.

(  4  ) É um tipo de lock.

(  2  ) Uma consulta dentro de outra consulta.

(  5  ) Normalmente encontrado em operações de controle de concorrência.

(  3  ) Uma espécie de tabela virtual utilizada para simplificar a legibilidade do código e tornar colunas dinâmicas em colunas filtráveis.


-- 11
Junta as tabelas venda e vendedor com base no codigo (da venda)
pega dados dos dois e coloca em uma tabela temporaria

recupera essa tabela temporaria 
adicionando uma coluna ao final de cada linha:
    soma total dos valores de venda 
    separando por cada categoria a soma 
    ordenado pela data da venda crescente 
    e coloca o nome da coluna de "valor" 
e depois filtra tudo para pegar apenas as vendas de 2024

P.S 
    lembrando que esse sum não funciona como GROUP BY por se tratar de uma função de janela
    ele apenas adiciona uma coluna no final 
    Por ele não definir ROWS BETWEEN a soma vai ficar evidente como ex: 
    categoria   val soma
    categorria1 2RS 2
    categorria1 2RS 4
    categorria1 2RS 6

    categorria2 2RS 2
    categorria2 2RS 4
    categorria2 2RS 6
    isso porque ele vai somando conforme passa por cada categoria e salva o valor total até o momento na linha

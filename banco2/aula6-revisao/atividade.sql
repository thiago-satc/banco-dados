-- 1 
CREATE TABLE resultado_natacao (
    nome_atleta varchar(255),
    data_competicao date,
    tempo numeric(5,2),
    modalidade varchar(50)
);

INSERT INTO resultado_natacao values 
    ( 'a', '2023-09-17' , 10.1, '0'),
    ( 'a1', '2023-09-17' , 10.1, '1'),
    ( 'b1', '2023-09-17' , 10.2, '1'),
    ( 'c1', '2023-09-17' , 10.3, '1'),
    ( 'a2', '2023-09-17' , 10.1, '2'),
    ( 'b2', '2023-09-17' , 10.2, '2'),
    ( 'c2', '2023-09-17' , 10.3, '2'),
    ( 'c', '2023-09-17' , 10.3, '0'),
    ( 'b', '2023-09-17' , 10.2, '0')
;

-- Resposta
WITH cte AS (
	SELECT 
		modalidade, 
		nome_atleta, 
		tempo,
		DENSE_RANK() OVER(PARTITION BY modalidade ORDER BY modalidade, tempo) AS colocacao
	FROM resultado_natacao
	WHERE data_competicao = '2023-09-17'
)

SELECT * FROM cte
WHERE colocacao <= 3


-- 2
-- Bloco de operações que executam de maneira integral, ou seja se algum erro acontecer no meio desse bloco, ou se for Informado um ROLLBACK
-- tudo que foi feito é desfeito

-- 3 Marque VERDADEIRO ou FALSO para as afirmações abaixo e depois escolha a alternativa que corresponde a sequência

( V ) O shared lock, em uma transação, permite que várias outras transações leiam os mesmos dados ao mesmo tempo, mas
impede que outras transações atualizem ou excluam esses dados.
( V ) O exclusive lock, em uma transação, impede que outras transações acessem ou alterarem os dados dessa transação.
( V ) Quando um objeto dentro de uma transação recebe um exclusive lock, é possível liberar o lock deste objeto sem que a
transação tenha sido finalizada.
( V ) Quando um objeto dentro de uma transação recebe um shared lock, é possível liberar o lock deste objeto sem que a
transação tenha sido finalizada.
( F ) Os shared lock e exclusive lock só são liberado quando uma transação (implícita ou explícita) é finalizada.

-- 4 Como o uso de índices pode ajudar a minimizar problemas de bloqueio e melhorar a performance no SQL Server? Explique com suas palavras.
os indicis agilizam os processos de buscas, criando arvores binarias para agilizar a pesquisa dos dados.

-- 5 Marque VERDADEIRO ou FALSO para as afirmações abaixo e depois escreva a sua resposta na sequência correta
referente à junções de tabelas em bancos de dados:
(V ) O INNER JOIN retorna todas as linhas de ambas as tabelas que têm valores correspondentes nas colunas de junção
especificadas.
(V ) O LEFT JOIN entre duas tabelas retorna todas as linhas da tabela à esquerda e as linhas correspondentes da tabela à
direita. Se não houver correspondência, NULL é retornado para as colunas da tabela à direita.
(V ) O RIGHT JOIN é similar a um LEFT JOIN, mas ele inverte a ordem das tabelas, retornando todas as linhas da tabela à
direita e as linhas correspondentes da tabela à esquerda.
( V) O FULL JOIN entre duas tabelas retorna todas as linhas de ambas as tabelas, incluindo as que não têm correspondência.
As colunas da tabela sem correspondência são preenchidas com NULL.
( V) O CROSS JOIN (também conhecido como produto cartesiano) combina cada linha da primeira tabela com todas as linhas
da segunda tabela, criando um conjunto de resultados que contém todas as combinações possíveis entre os registros as duas
tabelas, sem levar em consideração qualquer critério de correspondência específico.

-- 6
Marque VERDADEIRO ou FALSO para as afirmações abaixo e depois escreva sua resposta na sequência correta
referente a índices em bancos de dados:
(F ) É possível ter mais um índice clustered por tabela, desde que a chave do índice seja diferente.
( F) Não é possível criar dois índices nonclustered usando a mesma chave (mesmo coluna para indexação), mesmo que
tenham nomes de índices diferentes.
( V ) Um índice clustered pode ter mais de uma coluna como chave.
( F ) A sintaxe - create index idx_data_competicao on resultado_natacao (data_competicao) - cria um índice chamado
data_competicao na tabela resultado_natacao, usando a coluna idx_data_competicao como chave deste índice.
( V ) No nível folha de um índice clustered, encontramos todos as colunas da tabela, independente da(s) coluna(s) escolhida(s)
como chave.
( F ) No nível folha de um índice nonclustered, encontramos todos as colunas da tabela, independente da(s) coluna(s)
escolhida(s) como chave.

-- 7 

CREATE INDEX idx ON resultado_natacao (data_competicao) INCLUDE (modalidade, nome_atleta, tempo);

-- 8
código do agendamento, nome do paciente, nome
do profissional, data de início e fim do agendamento somente do ano de 2020, ordenado de forma ascendente pela data de
início dos agendamentos

SELECT ag.cd_agenda, pa.nm_paciente, pr.nm_profissional, ag.dt_inicio, ag.dt_fim 
FROM agenda
LEFT JOIN paciente ON 
WHERE ag.dt_inicio BETWEEN '2020-01-01' AND '2020-12-31' 
AND ag.dt_fim BETWEEN '2020-01-01' AND '2020-12-31'
ORDER BY ag.dt_inicio


-- 9 
SELECT nm_profissional 
FROM profissional a
WHERE NOT EXISTS (
    SELECT 1 
    FROM agenda b
    WHERE dt_inicio BETWEEN '2021-01-01' AND '2021-12-31'
        AND dt_fim BETWEEN '2021-01-01' AND '2021-12-31'
        AND b.cd_profissional = a.cd_profissional
) 